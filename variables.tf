variable "key_name" {
  default     = ""
  type        = string
  description = "Key pair name for instance"
}

variable "subnet_id" {
  default     = ""
  type        = string
  description = "Subnet ID for instance"
}

variable "sg_id" {
  default     = ""
  type        = string
  description = "Security Group ID for instance"
}

variable "instance_type" {
  default     = "t2.micro"
  type        = string
  description = "Instance Type for instance"
}

variable "ami" {
  default     = ""
  type        = string
  description = "AMI for instance"
}

variable "instance_name" {
  default     = ""
  type        = string
  description = "Instance Name for instance"
}